﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace MeteorShowerCSharp
{
    public class GameObject
    {
        // Version 2
        // Includes methods for moving, bouncing and drawing,
        // Including drawing a bitmap image
        // move method made "virtual" to allow overriding in subclasses

        public int xspeed = 10;
        public int yspeed = 10;
        public int x=0;
        public int y=0;
        public Brush colour = Brushes.Red;
        public int size=10;
        public Bitmap img;
        public bool imageset;
        public bool inplay = true;


        public void draw(Form f)
         {
             Graphics g = f.CreateGraphics();
             if(!this.imageset)
             {
                 g.FillEllipse(this.colour, this.x, this.y, this.size, this.size);
             } else {
                 g.DrawImage(this.img, this.x, this.y);
             }
             g.Dispose(); 
         }

        public virtual void draw(PaintEventArgs e)
        {
            if (!this.imageset)
            {
                e.Graphics.FillEllipse(this.colour, this.x, this.y, this.size, this.size);
            }
            else
            {
                e.Graphics.DrawImage(this.img, this.x, this.y);
            }
        }

        public virtual void move(Form f)
        {
            x = x + xspeed;
            y = y + yspeed;
            if (this.x + this.size >= f.ClientSize.Width || this.x <= 0) xspeed = -xspeed;
            if (this.y + this.size >= f.ClientSize.Height || this.y <= 0) yspeed = -yspeed;
        }

        public void setImg(Image newImg)
        {
            img = (Bitmap) newImg;
            imageset = true;
            this.size = img.Width;
        }

    }
}
