﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeteorShowerCSharp
{
    public partial class Form1 : Form
    {
        List<Meteor> meteors = new List<Meteor>();
        int meteorNum = 10;
        Timer t = new Timer();
        Random r = new Random();
        Player p = new Player();
        List<Bullet> bullets = new List<Bullet>();
        int maxBullets = 5;
        int score = 0;
        int shield = 100;
        int level = 1;

        public Form1()
        {
            InitializeComponent();
            t.Enabled = true;
            t.Interval = 100;
            this.t.Tick += new EventHandler(t_Tick);
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
            this.Paint += new PaintEventHandler(Form1_Paint);

            p.setImg(Resource1.starship); // https://www.dropbox.com/s/wxvyhrzqo515pfs/StarShip1.png?dl=0
            
            for (int i = 0; i < meteorNum; i++)
            {
                Meteor g = new Meteor();
                g.x = this.ClientSize.Width+r.Next(200);
                g.y = r.Next(200);
                g.xspeed = -10-r.Next(20);
                g.yspeed = 10;
                g.setImg(Resource1.meteor); // https://www.dropbox.com/s/rixa5vmewc8glut/meteor.png?dl=0
                meteors.Add(g);
            }
        }

        public void t_Tick(object sender, EventArgs e)
        {
            this.Refresh();
            foreach (Bullet b in bullets)
            {
                b.move(this);
                if (b.x > this.ClientSize.Width) b.inplay = false;

            }
            foreach (GameObject meteor in meteors)
            {
                meteor.move(this);
                if (touching(p, meteor)) shield = Math.Max(shield - 10, 0);
                foreach (Bullet b in bullets)
                {
                    if (touching(b, meteor) && b.inplay && meteor.inplay)
                    {
                        b.inplay = false;
                        meteor.inplay = false;
                        score += 10;
                    }
                }
            }

            prbShield.Value = shield;
            lblScore.Text = "Score: " + score;
            // Game over?
            if (shield == 0)
            {
                t.Stop();
                MessageBox.Show("Game over, man!");
            }
            else
            {
                // Remove bullets that have gone beyond form container
                List<Bullet> bs = new List<Bullet>();
                foreach (Bullet b in bullets)
                {
                    if (b.inplay)
                    {
                        bs.Add(b);
                    }
                }
                bullets = bs;
                List<Meteor> ms = new List<Meteor>();
                foreach (Meteor m in meteors)
                {
                    if (m.inplay)
                    {
                        ms.Add(m);
                    }
                }
                meteors = ms;
            }
        }

        public void Form1_Paint(Object sender, PaintEventArgs e)
        {
            foreach (GameObject g in meteors)
            {
                g.draw(e);
            }
            p.draw(e);
            foreach (Bullet b in bullets)
            {
                b.draw(e);
            }
        }

        public void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    p.moveUp();
                    break;
                case Keys.Right:
                    p.moveRight();
                    break;
                case Keys.Down:
                    p.moveDown();
                    break;
                case Keys.Left:
                    p.moveLeft();
                    break;
                case Keys.Space:
                    FireBullet();
                    break;
            }
        }

       public bool touching(GameObject g1, GameObject g2)
       {
           return (Math.Abs(g1.x - g2.x) < (g1.size + g2.size)/2 && Math.Abs(g1.y - g2.y) < (g1.size + g2.size)/2);
	   }

       public void FireBullet()
       {
           if (bullets.Count() < maxBullets)
           {
               Bullet b = new Bullet(p);
               bullets.Add(b);
           }
       }
    }
}
