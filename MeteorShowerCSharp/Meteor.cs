﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace MeteorShowerCSharp
{
    class Meteor : GameObject
    {
        // Version 1
        // Subclass of GameObject with one overridden method to get the objects to move differently
        
        public override void move(Form f)
        {
            this.x = this.x + xspeed;
            if (this.x >= f.ClientSize.Width && xspeed>0) this.x = -this.size;
            if (this.x <= 0 && xspeed < 0) this.x = f.ClientSize.Width;
        }

        public override void draw(PaintEventArgs e)
        {
            base.draw(e);
            this.img.RotateFlip(RotateFlipType.Rotate90FlipNone);

        }
    }
}
