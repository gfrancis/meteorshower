﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace MeteorShowerCSharp
{
    class Bullet:GameObject
    {
        public int speed=10;
        private int pulsestate = 0;

        public Bullet(Player p)
        {
            this.x = p.x + p.size + 5;
            this.y = p.y + p.size / 2;
            this.colour = Brushes.Chartreuse;
            this.size = 10;
            this.pulsestate = 0;
            this.speed = 10;
            this.inplay = true;
        }
        public bool isFired()
        {
            return this.inplay;
        }
        
        public void stop()
        {
            inplay = false;
        }

        public void setSpeed(int newspeed)
        {
            this.speed = newspeed;
        }

        public override void move(Form f)
        {
            this.x = this.x + this.speed;
            this.pulse();
        }

        public void pulse()
        {
            switch (pulsestate)
            {
                case 0:
                    this.size = 14;
                    this.pulsestate = 1;
                    break;
                case 1:
                    this.size = 10;
                    this.pulsestate = 0;
                    break;
            }
        }

    }
}
