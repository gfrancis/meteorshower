﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeteorShowerCSharp
{
    class Player : GameObject
    {
        // Version 1
        // Subclass of GameObject. Moves up, down, left or right
        
        int increment = 20;

        public void moveUp()
        {
            this.y = this.y - increment;
        }

        public void moveRight()
        {
            this.x = this.x + increment;
        }

        public void moveDown()
        {
            this.y = this.y + increment;
        }

        public void moveLeft()
        {
            this.x = this.x - increment;
        }
    }
}
