﻿namespace MeteorShowerCSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblScore = new System.Windows.Forms.Label();
            this.prbShield = new System.Windows.Forms.ProgressBar();
            this.lblShield = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(12, 12);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(47, 13);
            this.lblScore.TabIndex = 0;
            this.lblScore.Text = "Score: 0";
            // 
            // prbShield
            // 
            this.prbShield.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prbShield.Location = new System.Drawing.Point(420, 12);
            this.prbShield.Name = "prbShield";
            this.prbShield.Size = new System.Drawing.Size(100, 13);
            this.prbShield.TabIndex = 1;
            this.prbShield.Value = 100;
            // 
            // lblShield
            // 
            this.lblShield.AutoSize = true;
            this.lblShield.Location = new System.Drawing.Point(378, 12);
            this.lblShield.Name = "lblShield";
            this.lblShield.Size = new System.Drawing.Size(36, 13);
            this.lblShield.TabIndex = 2;
            this.lblShield.Text = "Shield";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 423);
            this.Controls.Add(this.lblShield);
            this.Controls.Add(this.prbShield);
            this.Controls.Add(this.lblScore);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "MeteorShower";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.ProgressBar prbShield;
        private System.Windows.Forms.Label lblShield;

    }
}

